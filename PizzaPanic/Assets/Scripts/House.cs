﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {

    [HideInInspector]
    public DeliveryManager manager_;
    public GameObject pizzaPointPrefab;
    private bool hasOrder_;

    private InteractionPoint interactionPoint_;

    private GameObject markerHolder_;


	// Use this for initialization
	void Start ()
    {
        manager_ = GameObject.Find("DeliveryManager").GetComponent<DeliveryManager>();
        hasOrder_ = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (hasOrder_)
        {

            if (interactionPoint_.activated == true)
            {
                if (interactionPoint_.driver.hasPizza)
                {
                    OrderDone();
                }
            }
        }
    }

    public void PlaceOrder()
    {
        hasOrder_ = true;

        markerHolder_ = Instantiate(pizzaPointPrefab, transform);
        interactionPoint_ = markerHolder_.GetComponent<InteractionPoint>();
    }

    void OrderDone()
    {
        interactionPoint_.driver.DeliverPizza(this);
        interactionPoint_.activated = false;
        Destroy(markerHolder_);
        manager_.PizzaDelivered(this);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(transform.position, new Vector3(2, 2, 2));
    }
}
