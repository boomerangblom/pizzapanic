﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Driver : MonoBehaviour {

    private Text uiText;
    private Bike bike_;

    [HideInInspector]
    public bool hasPizza;
    private GameObject pizza_;

    public int money;
    public float pizzaCooldownTime;

    public Material mat;

    private int slicesLeft_;
    private float sliceWarmth_;

	// Use this for initialization
	void Start ()
    {
        money = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.name == "Pizza")
            {
                pizza_ = child.gameObject;
            }
        }
    }

    public void Initialise()
    {
        bike_ = GetComponent<Bike>();
        string uiNameHolder = "Player" + bike_.playerNumber.ToString() + "Text";
        uiText = GameObject.Find(uiNameHolder).GetComponent<Text>();
        uiText.color = mat.color;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(hasPizza)
        {
            sliceWarmth_ -= (Time.deltaTime);
            
            if(sliceWarmth_ <= 0.0f)
            {
                pizza_.transform.GetChild(slicesLeft_ - 1).gameObject.SetActive(false);
                slicesLeft_--;
                sliceWarmth_ = pizzaCooldownTime / 6;
                if (slicesLeft_ == 0)
                {
                    LosePizza(null);
                }
            }
        }

        //------------------------------------ UI STUFF

        uiText.text = bike_.playerNumber.ToString() + "\n" + "$" + money.ToString();
	}

    public void PickupPizza()
    {
        hasPizza = true;
        sliceWarmth_ = pizzaCooldownTime/6;
        slicesLeft_ = 6;
        pizza_.SetActive(true);
        for (int i = 0; i < pizza_.transform.childCount; i++)
        {
            Transform child = pizza_.transform.GetChild(i);
            child.gameObject.SetActive(true);
        }
    }

    public void DeliverPizza(House deliveredTo)
    {
        money += (int)(10 + slicesLeft_);
        hasPizza = false;
        pizza_.SetActive(false);
    }

    public void LosePizza(Collision collision)
    {
        if (hasPizza)
        {
            for (int i = 0; i < slicesLeft_; i++)
            {
                Transform ogSlice = pizza_.transform.GetChild(i);
                Transform newSlice = Instantiate(ogSlice);
                newSlice.transform.position = ogSlice.transform.position;
                newSlice.transform.localScale = ogSlice.transform.lossyScale;
                Rigidbody sliceRigidbody = newSlice.gameObject.AddComponent<Rigidbody>();
                MeshCollider collider = newSlice.gameObject.AddComponent<MeshCollider>();
                collider.convex = true;
                sliceRigidbody.velocity = -collision.relativeVelocity * 0.3f + new Vector3(0, 20, 0);
                sliceRigidbody.useGravity = true;
            }
            money -= 10;
            hasPizza = false;
            pizza_.SetActive(false);
        }
    }
}
