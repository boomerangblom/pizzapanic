﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public List<GameObject> players;
    private MenuManager menuManager_;

	// Use this for initialization
	void Start () {
        players = new List<GameObject>();

        players.Add(GameObject.Find("Player1"));
        players.Add(GameObject.Find("Player2"));
        players.Add(GameObject.Find("Player3"));
        players.Add(GameObject.Find("Player4"));

        GameObject menuManagerGameObject = GameObject.Find("Manager");
        if (menuManagerGameObject != null)
        {
            menuManager_ = menuManagerGameObject.GetComponent<MenuManager>();

            for (int i = players.Count - 1; i >= 0; i--)
            {
                if (menuManager_.players[i].connected)
                {
                    if (menuManager_.players[i].altControls)
                    {
                        players[i].GetComponent<Bike>().altControls = true;
                    }
                    else
                    {
                        players[i].GetComponent<Bike>().altControls = false;
                    }
                }
                else
                {
                    Destroy(players[i]);
                    players.RemoveAt(i);
                }
            }
        }
    }
}
