﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaShop : MonoBehaviour {

    public GameObject pizzaPointPrefab;

    private DeliveryManager manager_;
    private InteractionPoint interactionPoint_;

    private GameObject markerHolder_;

    private bool hasPizza_;

	// Use this for initialization
	void Start ()
    {
        manager_ = GameObject.Find("DeliveryManager").GetComponent<DeliveryManager>();
        hasPizza_ = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(hasPizza_)
        {
            if (interactionPoint_.activated == true)
            {
                if (interactionPoint_.driver.hasPizza == false)
                {
                    interactionPoint_.driver.PickupPizza();
                    PizzaPickedUp();
                }
            }
        }
	}

    public void MakePizza()
    {
        markerHolder_ = Instantiate(pizzaPointPrefab, transform);
        interactionPoint_ = markerHolder_.GetComponent<InteractionPoint>();
        hasPizza_ = true;
    }

    void PizzaPickedUp()
    {
        Destroy(markerHolder_);
        hasPizza_ = false;
        manager_.PizzaPickedUp(this);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawCube(transform.position, new Vector3(2, 2, 2));
    }
}
