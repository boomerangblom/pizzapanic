﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{
    [System.Serializable]
    public struct Player
    {
        public bool connected;
        public int number;
        public bool altControls;
    }
    public Player[] players = new Player[4];
    public int playersJoined = 0;

    private GameObject startToPlayText_;
    private GameObject[] controllers = new GameObject[4];

    private bool gameStart_;

    private void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("MenuManager").Length == 1)
        {
            for (int i = 0; i < players.Length; i++)
            {
                players[i].connected = false;
                players[i].altControls = false;
                players[i].number = i;
            }
            SceneManager.sceneLoaded += OnLevelLoaded;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "MainMenu")
        {
            startToPlayText_ = GameObject.Find("StartToPlayText");
            startToPlayText_.SetActive(false);
            controllers[0] = GameObject.Find("PL1Control");
            controllers[1] = GameObject.Find("PL2Control");
            controllers[2] = GameObject.Find("PL3Control");
            controllers[3] = GameObject.Find("PL4Control");
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].connected)
                {
                    controllers[i].SetActive(true);
                    if (players[i].altControls == true)
                    {
                        controllers[i].transform.Find("AlternativeControl").gameObject.SetActive(true);
                    }
                }
                else
                {
                    controllers[i].SetActive(false);
                }
            }
            gameStart_ = false;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!gameStart_)
        {
            CheckButtons();

            if (playersJoined >= 2)
            {
                startToPlayText_.SetActive(true);
            }
            else if (playersJoined < 2)
            {
                startToPlayText_.SetActive(false);
            }
            if (startToPlayText_.activeSelf == true && Input.GetButtonDown("Confirm"))
            {
                gameStart_ = true;
                DontDestroyOnLoad(this);
                SceneManager.LoadScene(1);
            }
        }
    }

    private void CheckButtons()
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (Input.GetButtonDown(i+1 + "X") || Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                TogglePlayer(i);
            }
            if (Input.GetButtonDown(i+1 + "Triangle"))
            {
                ToggleAltControl(i);
            }
        }
    }

    void TogglePlayer(int number)
    {
        if (players[number].connected)
        {
            players[number].connected = false;
            players[number].altControls = false;
            controllers[number].transform.Find("AlternativeControl").gameObject.SetActive(false);
            controllers[number].SetActive(false);
            playersJoined--;
        }
        else
        {
            players[number].connected = true;
            controllers[number].SetActive(true);
            playersJoined++;
        }
    }

    void ToggleAltControl(int playerNumber)
    {
        if (players[playerNumber].connected)
        {
            if (players[playerNumber].altControls == true)
            {
                players[playerNumber].altControls = false;
                controllers[playerNumber].transform.Find("AlternativeControl").gameObject.SetActive(false);
            }
            else
            {
                players[playerNumber].altControls = true;
                controllers[playerNumber].transform.Find("AlternativeControl").gameObject.SetActive(true);
            }
        }
    }
}
