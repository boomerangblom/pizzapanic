﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryManager : MonoBehaviour {
    private Driver[] drivers_;

    private List<PizzaShop> availableShops_ = new List<PizzaShop>();
    private List<House> availableHouses_ = new List<House>();


    private int pizzasSpawned_;
    private int housesSpawned_;

    // Use this for initialization
    void Start ()
    {
        pizzasSpawned_ = 0;
        GameObject[] houses_ = GameObject.FindGameObjectsWithTag("House");
        for (int i = 0; i < houses_.Length; i++)
        {
            availableHouses_.Add(houses_[i].GetComponent<House>());
        }
        GameObject[] pizzaShops_ = GameObject.FindGameObjectsWithTag("Shop");
        for (int i = 0; i < pizzaShops_.Length; i++)
        {
            availableShops_.Add(pizzaShops_[i].GetComponent<PizzaShop>());
        }

        PlayerManager playManager = GameObject.Find("EventSystem").GetComponent<PlayerManager>();
        GameObject[] players_ = new GameObject[playManager.players.Count];

        for (int i = 0; i < playManager.players.Count; i++)
        {
            players_[i] = playManager.players[i];
        }

        drivers_ = new Driver[players_.Length];
        for (int i = 0; i < players_.Length; i++)
        {
            drivers_[i] = players_[i].GetComponent<Driver>();
            drivers_[i].Initialise();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        MakePizzasNeeded();
	}

    void MakePizzasNeeded()
    {
        int driverPizzas = 0;
        for (int i = 0; i < drivers_.Length; i++)
        {
            if (drivers_[i].hasPizza == true)
            {
                driverPizzas++;
            }
        }
        int pizzasNeeded = drivers_.Length - driverPizzas - pizzasSpawned_;
        for (int i = 0; i < pizzasNeeded; i++)
        {
            MakeRandomPizza();
        }
    }

    public void PizzaPickedUp(PizzaShop shop)
    {
        pizzasSpawned_--;
        availableShops_.Add(shop);
        MakeOrdersNeeded();
    }

    public void PizzaDelivered(House house)
    {
        availableHouses_.Add(house);
        housesSpawned_--;
    }

    private void MakeRandomPizza()
    {
        if (availableShops_.Count > 0)
        {
            int shopNumber = Random.Range(0, availableShops_.Count);
            availableShops_[shopNumber].MakePizza();
            availableShops_.RemoveAt(shopNumber);
            pizzasSpawned_++;
        }
    }

    private void MakeOrdersNeeded()
    {
        int driverPizzas = 0;
        for (int i = 0; i < drivers_.Length; i++)
        {
            if (drivers_[i].hasPizza == true)
            {
                driverPizzas++;
            }
        }
        int ordersNeeded = driverPizzas - housesSpawned_;
        for (int i = 0; i < ordersNeeded; i++)
        {
            MakeRandomOrder();
        }
    }

    private void MakeRandomOrder()
    {
        if (availableHouses_.Count > 0)
        {
            int houseNumber = Random.Range(0, availableHouses_.Count);
            availableHouses_[houseNumber].PlaceOrder();
            availableHouses_.RemoveAt(houseNumber);
            housesSpawned_++;
        }
    }
}
