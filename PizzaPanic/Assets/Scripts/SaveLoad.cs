﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public static class SaveLoad
{
    public static bool FileExists(string relativePath)
    {
        bool exists = false;
        if(File.Exists(Application.dataPath + relativePath))
        {
            exists = true;
        }
        return exists;
    }

    public static void Save<T> (T value, string relativePath)
    {
        BinaryFormatter bf = new BinaryFormatter();

        
        File.SetAttributes(Application.dataPath + relativePath, File.GetAttributes(Application.dataPath + relativePath) & ~FileAttributes.ReadOnly);

        FileStream file = new FileStream(Application.dataPath + relativePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);//        File.OpenWrite(Application.dataPath + relativePath);


        bf.Serialize(file, value);
        file.Close();


//        FileStream logFileStream = new FileStream("c:\test.txt", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
//        StreamReader logFileReader = new StreamReader(logFileStream);

    }

    public static T Load<T>(string relativePath)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.dataPath + relativePath);
        T cachedFile = (T)bf.Deserialize(file);
        file.Close();
        return cachedFile;
    }

    public static void XmlSave<T> (T value, string relativePath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        using (FileStream stream = new FileStream(Application.dataPath + relativePath, FileMode.Create))
        {
            serializer.Serialize(stream, value);
            stream.Close();
        }
    }

    public static T XmlLoad<T>(string relativePath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        using (FileStream stream = new FileStream(Application.dataPath + relativePath, FileMode.Open))
        {
            T cachedFile = (T)serializer.Deserialize(stream);
            stream.Close();
            return cachedFile;
        }
    }

}