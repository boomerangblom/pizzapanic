﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowShrinkDisappear : MonoBehaviour {
    
    public float timeGrowing;
    public float timeShrinking;
    public float timeStable;

    private float timer_;

    private bool growing_ = true;
    private bool shrinking_ = false;
    private Vector3 originalScale;

	// Use this for initialization
	void Start ()
    {
        originalScale = transform.localScale;
        transform.localScale = Vector3.zero;
        timer_ = timeShrinking;
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer_ -= Time.deltaTime;
		if (timer_ <= 0)
        {
            if (growing_)
            {
                growing_ = false;
                timer_ = timeStable;
            }
            else if (!shrinking_)
            {
                shrinking_ = true;
                timer_ = timeShrinking;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        if (growing_)
        {
            float newScale = Mathf.Lerp(0, originalScale.z, 1 - (timer_ / timeGrowing));
            transform.localScale = new Vector3(newScale, 0.1f, newScale);
        }
        else if (shrinking_)
        {
            float newScale = Mathf.Lerp(originalScale.z, 0, 1 - (timer_ / timeShrinking));
            transform.localScale = new Vector3(newScale, 0.1f, newScale);
        }
	}
}
