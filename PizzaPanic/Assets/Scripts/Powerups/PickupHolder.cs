﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHolder : MonoBehaviour {

    public Pickup isHoldingPickup = null;

    private PickupManager pickupManager_;
    private Bike bike_;
    // Use this for initialization
    void Start ()
    {
        pickupManager_ = GameObject.FindGameObjectWithTag("PickupManager").GetComponent<PickupManager>();
    }
	
    private void OnTriggerEnter(Collider other)
    {
        if (isHoldingPickup == false)
        {
            if (other.tag == "Pickup")
            {
                PickupOnMap picked_up = other.gameObject.GetComponent<PickupOnMap>();
                switch (picked_up.type)
                {
                    case Pickup.pickupType.OIL:
                        isHoldingPickup = gameObject.AddComponent<Oil>();
                        break;
                    case Pickup.pickupType.STAR:
                        isHoldingPickup = gameObject.AddComponent<Star>();
                        break;
                    default:
                        Debug.LogError("Picked up null");
                        break;

                }
                pickupManager_.PickedUpPickup(other.gameObject);
                Destroy(other.gameObject);
                UsePickup();
            }
        }
    }

    void UsePickup()
    {
        if (isHoldingPickup != null)
        {
            isHoldingPickup.Use();
        }
    }
}
