﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : Pickup
{
    public float timeInUse = 8.0f;
    public float newAcceleration = 170;
    public float newMaxSpeed = 100;
    
    private float oldAcceleration_;
    private float oldBoostAcceleration_;
    private float oldMaxSpeed_;
    private float oldBoostMaxSpeed_;

    private float timer_;

    private Bike ownerBike_;



	// Use this for initialization
	void Awake ()
    {
        ownerBike_ = GetComponent<Bike>();
        type = pickupType.STAR;
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer_ -= Time.deltaTime;
        if (timer_ <= 0)
        {
            StopEffect();
        }
	}

    public override void Use()
    {
        timer_ = timeInUse;
        
        oldAcceleration_ = ownerBike_.acceleration;
        oldBoostAcceleration_ = ownerBike_.boostAcceleration;
        oldMaxSpeed_ = ownerBike_.maxSpeed;
        oldBoostMaxSpeed_ = ownerBike_.boostMaxSpeed;
        
        ownerBike_.acceleration = newAcceleration;
        ownerBike_.boostAcceleration = newAcceleration;
        ownerBike_.maxSpeed = newMaxSpeed;
        ownerBike_.boostMaxSpeed = newMaxSpeed;
    }
    
    void StopEffect()
    {
        ownerBike_.acceleration = oldAcceleration_;
        ownerBike_.boostAcceleration = oldBoostAcceleration_;
        ownerBike_.maxSpeed = oldMaxSpeed_;
        ownerBike_.boostMaxSpeed = oldBoostMaxSpeed_;

        PickupUsedUp();
    }
}
