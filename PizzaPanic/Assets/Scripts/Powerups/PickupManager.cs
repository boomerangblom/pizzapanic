﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
    public float minPickupSpawnTime;
    public float maxPickupSpawnTime;
    public int maxPickupsAtOnce;


    public GameObject[] pickupPrefabs_;
    private List<GameObject> availableSpawnPoints_;

    private float timer_;
    private int PickupsOnMap_;

	// Use this for initialization
	void Start ()
    {
        availableSpawnPoints_ = new List<GameObject>(GameObject.FindGameObjectsWithTag("PickupPoint"));
        timer_ = Random.Range(minPickupSpawnTime, maxPickupSpawnTime);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnPickup();
        }
        timer_ -= Time.deltaTime;
        if (timer_ <= 0 && PickupsOnMap_ < maxPickupsAtOnce)
        {
            SpawnPickup();
            timer_ = Random.Range(minPickupSpawnTime, maxPickupSpawnTime);
        }
	}

    void SpawnPickup()
    {
        if (availableSpawnPoints_.Count > 0)
        {
            int pickupType = Random.Range(0, pickupPrefabs_.Length);
            int spawnPoint = Random.Range(0, availableSpawnPoints_.Count - 1);

            GameObject.Instantiate(pickupPrefabs_[pickupType], availableSpawnPoints_[spawnPoint].transform);
            availableSpawnPoints_.RemoveAt(spawnPoint);
            PickupsOnMap_++;
        }
    }

    public void PickedUpPickup(GameObject pickup)
    {
        if (pickup.transform.parent != null)
        {
            if (pickup.transform.parent.tag == "PickupPoint")
            {
                availableSpawnPoints_.Add(pickup.transform.parent.gameObject);
                PickupsOnMap_--;
            }
        }
    }
}
