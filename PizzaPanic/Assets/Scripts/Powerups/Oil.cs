﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oil : Pickup
{
    private bool in_use = false;
    private int chargesLeft_ = 12;
    private float timeBetweenCharges = 0.1f;
    private float timer_;

    public void Awake()
    {
        type = pickupType.OIL;
    }

    public override void Use()
    {
        in_use = true;
        timer_ = timeBetweenCharges;
    }

    private void Update()
    {
        if (in_use)
        {
            if (chargesLeft_ == 0)
            {
                PickupUsedUp();
            }
            timer_ -= Time.deltaTime;
            if (timer_ < 0)
            {
                timer_ = timeBetweenCharges;
                SpawnOilSplat();
            }
        }
    }

    private void SpawnOilSplat()
    {
        RaycastHit hitInfo;
        Physics.Raycast(transform.position, Vector3.down, out hitInfo, 1000.0f, LayerMask.GetMask(new string[] { "Floor" }));
        Instantiate(Resources.Load("OilSplat"), hitInfo.point, Quaternion.identity);
        chargesLeft_--;
    }
}
