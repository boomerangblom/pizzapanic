﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public enum pickupType
    {
        NULL,
        OIL,
        STAR
    }
    public pickupType type;
    public abstract void Use();



    protected void PickupUsedUp()
    {
        Destroy(this);
        GetComponent<PickupHolder>().isHoldingPickup = null;
    }
}
