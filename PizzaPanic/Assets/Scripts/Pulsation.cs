﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsation : MonoBehaviour
{
    public float rotationSpeed;
    public float pulsationSpeed;
    public float pulsationAmount;
    Vector3 startingScale;
	// Use this for initialization
	void Start ()
    {
        startingScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.localScale = startingScale += new Vector3(Mathf.Sin(Time.time * pulsationSpeed), 0, Mathf.Sin(Time.time * pulsationSpeed)) * pulsationAmount;
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
	}
}
