using UnityEngine;
using System.Collections;

public class ScreenShaker : MonoBehaviour {
    
    private static float shakeintensity;
    private static float shaketime;
    private static float shaketimemax;
    public static Vector3 basepos;
    
    static ScreenShaker() {
        basepos = Vector3.zero;
        Reset();
    }

	void Start () {
        if (basepos == Vector3.zero) basepos = transform.localPosition;
    }
	
	void Update () {
        // don't shake and reset shake values if screen shake is disabled
        /*if (!MenuInfo.GetInstance().screenShake) {
            Reset();
            return;
        }*/
        // keep track of how long we've been shaking
        if (shaketime <= 0f) {
            // if timer has expired, make sure the camera is set to its normal position
            transform.localPosition = basepos;
            return;
        }
        shaketime -= Time.unscaledDeltaTime;
        // calculate a random delta multiplied by the timer and the shake intensity
        float power = Mathf.Max(shaketime / shaketimemax, 0f) * shakeintensity;
        Vector3 delta = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * power;
        // apply delta to camera
        transform.localPosition = basepos + delta;
    }

    public static void Reset() {
        shaketime = 0f;
        shaketimemax = 1f; // avoid divide by zero
        shakeintensity = 0f;
    }

    public static void AddShake(float time, float intensity) {
        shaketime = time;
        shaketimemax = time;
        shakeintensity = intensity;
    }
}
