﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLoopController : MonoBehaviour
{
    public float timeUntilStart;
    public float gameTime;
    public float timeUntilStartMusic;

    private bool gameStarted_;
    private bool gameFinished_;
    private bool musicStarted_;

    private float currentTimer_;
    private float musicTimer_;

    private List<Bike> bikes_ = new List<Bike>();
    private GameObject[] beams;


    private Text StartTimer_;
    private Text GameTimer_;
    private Text PlayerWon_;

    private void Start()
    {
        StartTimer_ = GameObject.Find("StartTimer").GetComponent<Text>();
        GameTimer_ = GameObject.Find("GameTimer").GetComponent<Text>();
        PlayerWon_ = GameObject.Find("PlayerWonText").GetComponent<Text>();
        PlayerManager player_manager = GameObject.Find("EventSystem").GetComponent<PlayerManager>();

        for (int i = 0; i < player_manager.players.Count; i++)
        {
            bikes_.Add(player_manager.players[i].GetComponent<Bike>());
        }
        for (int i = 0; i < bikes_.Count; i++)
        {
            bikes_[i].hasControl = false;
        }
        currentTimer_ = timeUntilStart;
        musicTimer_ = timeUntilStartMusic;

        beams = GameObject.FindGameObjectsWithTag("Beam");
    }

    private void Update()
    {
        currentTimer_ -= Time.deltaTime;
        musicTimer_ -= Time.deltaTime;
        if (!gameStarted_)
        {
            StartTimer_.text = currentTimer_.ToString("F0");
        }
        else if (!gameFinished_)
        {
            GameTimer_.text = currentTimer_.ToString("F0");
        }
        else
        {
            if (Input.GetButtonDown("Confirm"))
            {
                SceneManager.LoadScene(0);
            }
        }
        if (musicTimer_ <= 0)
        {
            GameObject.Find("TransparencyBeat").GetComponent<TransparencyBeat>().StartMusicBounce();
        }
        if (currentTimer_ <= 0)
        {
            if (!gameStarted_)
            {
                //Start game
                for (int i = 0; i < bikes_.Count; i++)
                {
                    bikes_[i].hasControl = true;
                }
                currentTimer_ = gameTime;
                Destroy(StartTimer_.gameObject);
                gameStarted_ = true;
            }
            else
            {
                gameFinished_ = true;
                for (int i = 0; i < bikes_.Count; i++)
                {
                    bikes_[i].hasControl = false;
                }
                Driver winner = GetWinner();
                PlayerWon_.text = "Player " + winner.GetComponent<Bike>().playerNumber + " Won";
                PlayerWon_.color = winner.mat.color;
                GameObject.Find("OptionsToRestart").GetComponent<Text>().enabled = true;
            }
        }
        Color winnerColor = GetWinner().mat.color;
        foreach(GameObject beam in beams)
        {
            Renderer renderer = beam.GetComponent<MeshRenderer>();
            Material mat = renderer.material;
            mat.SetColor("_EmissionColor", winnerColor);
        }
    }

    Driver GetWinner()
    {
        int winScore = -999;
        Bike winner = null;
        for (int i = 0; i < bikes_.Count; i++)
        {
            Driver bikeDriver = bikes_[i].GetComponent<Driver>();
            if (winScore < bikeDriver.money)
            {
                winScore = bikeDriver.money;
                winner = bikes_[i];
            }
        }
        return winner.GetComponent<Driver>();
    }

}
