﻿using System;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;
using UnityEngine;

public class Bike : MonoBehaviour
{

    public int playerNumber;
    public bool altControls;
    [Header("Speed")]
    public float maxSpeed;
    public float boostMaxSpeed;
    public float speedFalloff;
    public float acceleration;
    public float boostAcceleration;
    public float rotationSpeed;
    [Header("Drift")]
    public float driftPercentage;
    public float driftFalloff;
    public float oilDriftPercentage;
    public float oilDriftFalloff;
    [Header("Audio")]
    public float maxEnginePitch;
    public float minEnginePitch;
    public float driftForMaxSound;
    public float driftForMinSound;
    public float maxdriftVolume;
    public float minDriftPitch;
    public float maxDriftPitch;
    public AudioClip bumpSound;
    public AudioClip crashSound;

    [Header("Other")]
    public float crashThreshold;
    public float crashTime;
    public float leaningAmount;
    public float velocityForFullRotation;

    public bool hasControl = true;
    private float forwardVelocity_;
    public Vector3 driftVelocity_;
    private Rigidbody rigidbody_;
    private float timeUntilHasControl_;
    private GameObject controlDisabledIndicator_;
    private Driver driver_;
    private AudioSource engineAudio_;
    private AudioSource screechAudio_;
    private AudioSource bumpAudio_;
    private PickupHolder pickupHolder_;


    string xAxisName_;
    string yAxisName_;
    string accelerationAxisName_;
    string xButtonName_;
    string circleButtonName_;
    string triangleButtonName_;
    string squareButtonName_;

    private float driftPercentage_;
    private float driftFalloff_;
    private float acceleration_;
    private float maxSpeed_;

    public int oilCount_;

    private float prevRot_;


    // Use this for initialization
    void Start ()
    {
        xAxisName_ = string.Concat(playerNumber.ToString(), "XAxis");
        yAxisName_ = string.Concat(playerNumber.ToString(), "YAxis");
        accelerationAxisName_ = string.Concat(playerNumber.ToString(), "AccelerationAxis");
        xButtonName_ = string.Concat(playerNumber.ToString(), "X");
        circleButtonName_ = string.Concat(playerNumber.ToString(), "Circle");
        triangleButtonName_ = string.Concat(playerNumber.ToString(), "Triangle");
        squareButtonName_ = string.Concat(playerNumber.ToString(), "Square");

        driver_ = GetComponent<Driver>();

        rigidbody_ = GetComponent<Rigidbody>();
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.tag == "DisabledIndicator")
            {
                controlDisabledIndicator_ = child.gameObject;
            }
        }
        driftPercentage_ = driftPercentage;
        driftFalloff_ = driftFalloff;

        AudioSource[] audioSources = GetComponents<AudioSource>();

        engineAudio_ = audioSources[0];
        screechAudio_ = audioSources[1];
        bumpAudio_ = audioSources[2];

        pickupHolder_ = GetComponent<PickupHolder>();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        prevRot_ = transform.rotation.eulerAngles.y;
        if (Input.GetButton(xButtonName_))
        {
            acceleration_ = boostAcceleration;
            maxSpeed_ = boostMaxSpeed;
        }
        else
        {
            acceleration_ = acceleration;
            maxSpeed_ = maxSpeed;
        }

        if (hasControl)
        {
            Accelerate();
            Rotate();
        }
        Move();
        UnfuckRigidbody();
        ControlSound();
	}

    void Update()
    {
        if (timeUntilHasControl_ > 0)
        {
            timeUntilHasControl_ -= Time.deltaTime;
            if (timeUntilHasControl_ < 0)
            {
                hasControl = true;
                controlDisabledIndicator_.SetActive(false);
                GamePad.SetVibration((PlayerIndex)playerNumber - 1, 0.0f, 0.0f);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            Crash(collision);
        }
        else
        {
            if (collision.impulse.magnitude > crashThreshold)
            {
                Crash(collision);
            }
            else
            {
                Bump(collision);
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.tag == "Oil")
        {
            oilCount_++;
            driftPercentage_ = oilDriftPercentage;
            driftFalloff_ = oilDriftFalloff;
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.transform.tag == "Oil")
        {
            oilCount_--;
            if (oilCount_ <= 0)
            {
                driftPercentage_ = driftPercentage;
                driftFalloff_ = driftFalloff;
            }
        }
    }

    private void Accelerate()
    {
        forwardVelocity_ += Input.GetAxis(accelerationAxisName_) * acceleration_ * Time.fixedDeltaTime * (1 - driftPercentage_);
        if (Mathf.Abs(Input.GetAxis(accelerationAxisName_)) < 0.5f)
        {
            forwardVelocity_ *= speedFalloff;
        }
        forwardVelocity_ = Mathf.Clamp(forwardVelocity_, -maxSpeed_, maxSpeed_);
    }

    private void Move()
    {
        //transform.position += transform.forward * Time.deltaTime * forwardVelocity_;
        //transform.position += driftVelocity_ * Time.deltaTime;
        rigidbody_.velocity = forwardVelocity_ * transform.forward + driftVelocity_;
        driftVelocity_ *= driftFalloff_;
    }

    //Rotate and drift calculations
    private void Rotate()
    {
        float rotation;
        if (!altControls)
        {
            rotation = Input.GetAxis(xAxisName_) * rotationSpeed * Time.fixedDeltaTime;
        }
        else
        {
            Quaternion new_rotation = Quaternion.AngleAxis(LeftStickToDegrees(), Vector3.up);
            float old_rotation_y = transform.rotation.eulerAngles.y;
            float new_rotation_y = new_rotation.eulerAngles.y;
            rotation = new_rotation_y - old_rotation_y;
            //To prevent rotating 359 degrees to the left instead of rotating 1 to the right
            if (rotation > 180)
            {
                rotation -= 360;
            }
            else if (rotation < -180)
            {
                rotation += 360;
            }
            rotation = Mathf.Clamp(rotation, -rotationSpeed * Time.fixedDeltaTime, rotationSpeed * Time.fixedDeltaTime);

        }
        
        rigidbody_.angularVelocity = new Vector3(0, Mathf.Deg2Rad * rotation / Time.fixedDeltaTime, 0);
        //transform.Rotate(new Vector3(0, rotation, 0));
        float drift_part = (driftPercentage_ * (rotation / (rotationSpeed * Time.fixedDeltaTime)) * (rotation / (rotationSpeed * Time.fixedDeltaTime)));
        driftVelocity_ += transform.forward * (drift_part * forwardVelocity_);
        forwardVelocity_ *= 1 - drift_part;
    }

    private void UnfuckRigidbody()
    {
        //rigidbody_.angularVelocity = Vector3.zero;
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    private void Lean()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, Input.GetAxis(xAxisName_) * forwardVelocity_ * -leaningAmount);
    }

    private void Crash(Collision collision)
    {
        if (pickupHolder_.isHoldingPickup == null || pickupHolder_.isHoldingPickup.type != Pickup.pickupType.STAR)
        {
            bumpAudio_.clip = crashSound;
            bumpAudio_.Play();
            bumpAudio_.volume = 1;
            controlDisabledIndicator_.SetActive(true);
            driftVelocity_ = collision.relativeVelocity.normalized * 15;
            forwardVelocity_ = 0;
            hasControl = false;
            timeUntilHasControl_ = crashTime;
            ScreenShaker.AddShake(0.5f, 1.0f);
            driver_.LosePizza(collision);
            GamePad.SetVibration((PlayerIndex)playerNumber - 1, 0.7f, 0.7f);
        }
        else
        {
            if (pickupHolder_.isHoldingPickup != null)
            {
                Bump(collision);
            }
        }
    }

    private void Bump(Collision collision)
    {
        //bumpAudio_.clip = bumpSound;
        //bumpAudio_.Play();
        //bumpAudio_.volume = collision.relativeVelocity.magnitude / 50;
        driftVelocity_ += collision.impulse * 1.5f;
        transform.position += collision.impulse.normalized * 0.2f;
        if (hasControl)
        {
            GamePad.SetVibration((PlayerIndex)playerNumber - 1, 0.1f, 0.1f);
            timeUntilHasControl_ = 0.3f;
        }
    }

    private void ControlSound()
    {
        engineAudio_.pitch = Mathf.Abs(Input.GetAxis(accelerationAxisName_)) * (maxEnginePitch - minEnginePitch) + minEnginePitch;
        float driftAmount = Vector3.Dot(driftVelocity_, transform.right);
        driftAmount = Mathf.Clamp(Mathf.Abs(driftAmount) - driftForMinSound, 0, driftForMaxSound);
        driftAmount = Mathf.Clamp01(driftAmount / driftForMaxSound) * maxdriftVolume;
        screechAudio_.volume = screechAudio_.volume * 0.85f + driftAmount * 0.15f;
        //screechAudio_.pitch = Mathf.Abs(driftAmount) * (maxDriftPitch - minDriftPitch) + minDriftPitch;
    }

    private float LeftStickToDegrees()
    {
        float x = Input.GetAxis(xAxisName_);
        float y = Input.GetAxis(yAxisName_);
        Vector3 joystickVector = new Vector3(x, y, 0);
        if (joystickVector.magnitude > 0.1)
        {
            float angle = Vector3.Angle(joystickVector, Vector3.up);
            if (x < 0)
            {
                angle = -angle;
            }
            angle += 90;
            angle = -angle;
            return angle;
        }
        else
        {
            return transform.rotation.eulerAngles.y;
        }
    }
}
