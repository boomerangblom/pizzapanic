﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparencyBeat : MonoBehaviour
{
    public float bpm;
    public Material pulsatingMaterial;
    public float minAlpha;
    public float startAlpha;
    public float alphaRizeSpeed;
    public float offset;

    private bool started_;
    private float secondsForBeat_;
    private float timeWhenStarted_;
    private int beatsPassed_;
    private float nextBeatTime_;

	// Use this for initialization
	void Start ()
    {
        SetAlpha(startAlpha / 255);
        secondsForBeat_ = 60 / bpm;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (started_)
        {
            SetAlpha(pulsatingMaterial.color.a + alphaRizeSpeed * Time.deltaTime);
            if (Time.time >= nextBeatTime_)
            {
                Beat();
            }
        }
    }

    public void StartMusicBounce()
    {
        if (!started_)
        {
            started_ = true;
            GameObject.Find("Music").GetComponent<AudioSource>().Play();
            timeWhenStarted_ = Time.time;
            beatsPassed_++;
            nextBeatTime_ = timeWhenStarted_ + secondsForBeat_ + offset;
        }
    }

    void Beat()
    {
        beatsPassed_++;
        nextBeatTime_ = timeWhenStarted_ + secondsForBeat_ * beatsPassed_ + offset;
        SetAlpha(minAlpha / 255);
    }

    void SetAlpha(float alpha)
    {
        pulsatingMaterial.color = new Color(pulsatingMaterial.color.r, pulsatingMaterial.color.g, pulsatingMaterial.color.b, Mathf.Clamp(alpha, 0, 1));
    }
}
